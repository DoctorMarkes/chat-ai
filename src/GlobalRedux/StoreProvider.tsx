import { Provider } from 'react-redux'
import { store } from './store'
import React, { FC } from 'react'

const StoreProvider: FC<{ children: React.ReactNode }> = ({ children }) => {
  return <Provider store={store}>{children}</Provider>
}

export default StoreProvider
