import { createSlice } from '@reduxjs/toolkit'
import { RootState } from '../store.tsx'
import { IChat } from '../../types/chat.ts'

const initialState: IChat[] = []

export const chatSlice = createSlice({
  name: 'chats',
  initialState,
  reducers: {
    setChats: (_state, { payload }) => {
      return payload
    },
    unshiftChats: (state, { payload }) => {
      state.unshift(payload.chat)
      return state
    },
    updateChats: (state, { payload }: { payload: IChat[] }) => {
      if (state.length === 0) {
        return payload
      }

      payload.forEach((payloadChat) => {
        const index = state.findIndex(
          (stateChat) => stateChat._id === payloadChat._id,
        )
        if (index !== -1) {
          state[index] = payloadChat
        } else {
          state.push(payloadChat)
        }
      })

      return state
    },
  },
})

export const { setChats, unshiftChats, updateChats } = chatSlice.actions

export const selectChats = (state: RootState) => state.chats
export default chatSlice.reducer
