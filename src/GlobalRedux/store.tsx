import { combineReducers, configureStore } from '@reduxjs/toolkit'
import { chatsApi } from '../services/chat'
import chatReducer from './slices/chatSlice'

const rootReducer = combineReducers({
  [chatsApi.reducerPath]: chatsApi.reducer,
  chats: chatReducer,
})

export const store = configureStore({
  reducer: rootReducer,
  middleware: (getDefaultMiddleware) =>
    getDefaultMiddleware().concat(chatsApi.middleware),
})

export type AppStore = typeof store
export type RootState = ReturnType<AppStore['getState']>
export type AppDispatch = AppStore['dispatch']
