import './styles/App.css'
import './styles/global.css'
import { ThemeProvider } from '@components/theme-provider.tsx'
import StoreProvider from '../GlobalRedux/StoreProvider.tsx'
import Sidebar from '@components/Sidebar.tsx'
import Navbar from '@components/Navbar.tsx'
import { ChatPage } from '../pages/chatPage/ui'
import { SuspenseLayout } from '../shared/ui/layouts'

function App() {
  return (
    <ThemeProvider>
      <StoreProvider>
        <SuspenseLayout>
          <div className={'h-full relative'}>
            <div
              className={
                'hidden h-full md:w-72 md:flex md:flex-col md:fixed md:inset-y-0 z-[80] bg-gray-900'
              }
            >
              <Sidebar />
            </div>
            <main className={'md:pl-72'}>
              <Navbar />
              <ChatPage />
            </main>
          </div>
        </SuspenseLayout>
      </StoreProvider>
    </ThemeProvider>
  )
}

export default App
