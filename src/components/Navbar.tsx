import { UserButton } from '@clerk/clerk-react'
import MobileSidebar from '@components/mobile-sidebar'

const Navbar = () => {
  return (
    <div
      className={
        'flex w-full md:w-[calc(100%-18rem)] z-10 bg-black items-center p-4 fixed'
      }
    >
      <MobileSidebar />
      <div className={'flex ml-auto'}>
        <UserButton afterSignOutUrl={''} />
      </div>
    </div>
  )
}

export default Navbar
