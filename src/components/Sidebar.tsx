import { cn } from '@lib/utils'
import { NavLink } from 'react-router-dom'
import { LucideIcon, SquarePen } from 'lucide-react'
import ArchiveIcon from '@mui/icons-material/Archive'
import {
  useArchiveChatMutation,
  useFetchChatsCountQuery,
  useFetchChatsQuery,
} from '../services/chat.ts'
import { IChat } from '../types/chat.ts'
import InfiniteScroll from 'react-infinite-scroll-component'
import React, { useEffect, useMemo, useState } from 'react'
import { Tooltip, Zoom } from '@mui/material'
import Skeleton from '@mui/material/Skeleton'
import { useAppDispatch, useAppSelector } from '../GlobalRedux/hook.ts'
import {
  selectChats,
  setChats,
  updateChats,
} from '../GlobalRedux/slices/chatSlice.ts'
import { v4 as uuidv4 } from 'uuid'

export interface IChatItemMenu extends Partial<IChat> {
  icon: LucideIcon
  color: string
}

const createChatItemMenu: IChatItemMenu = {
  title: 'New chat',
  hash: '',
  icon: SquarePen,
  color: 'text-sky-500',
}

const Sidebar = () => {
  const [offsetPage, setOffsetPage] = useState(0)
  const chats = useAppSelector(selectChats)
  const dispatch = useAppDispatch()
  const {
    data = [],
    isLoading,
    isFetching,
  } = useFetchChatsQuery({ offsetPage })
  const { data: chatsCount = 0 } = useFetchChatsCountQuery()
  const [archiveChat] = useArchiveChatMutation()

  useEffect(() => {
    if (!isFetching && data.length) {
      dispatch(updateChats(data))
    }
  }, [data, dispatch, isFetching])

  const handleArchive = async (
    e: React.MouseEvent<SVGSVGElement, MouseEvent>,
    id: string,
  ) => {
    e.preventDefault()
    await archiveChat(id)
    dispatch(setChats(chats.filter((chat) => chat._id !== id)))
  }

  const id = useMemo(() => uuidv4(), [])

  return (
    <div
      className={
        'space-y-4 py-4 flex flex-col h-full bg-[#111827] text-white w-72'
      }
    >
      <div className={'px-0 py-2 flex-1 h-full'}>
        <NavLink to={'/chats'} className={'flex items-center pl-3 mb-4'}>
          <div className={'relative w-8 h-8 mr-4'}>
            <img src={'/logo.png'} alt={'Logo'} />
          </div>
          <h1 className={cn('text-2xl font-bold')}>Genius</h1>
        </NavLink>

        <div className={'px-2'}>
          <NavLink
            className={({ isActive }) =>
              cn(
                'text-md group flex p-2 w-full justify-start font-medium cursor-pointer hover:text-white hover:bg-white/10 rounded-lg transition',
                isActive ? 'text-white bg-white/10' : 'text-zinc-400',
              )
            }
            to={'/chats'}
            end
          >
            <div className={'flex items-center flex-1'}>
              {createChatItemMenu.title}
              {createChatItemMenu?.icon && (
                <createChatItemMenu.icon
                  className={cn('p-0.5 ml-auto', createChatItemMenu.color)}
                />
              )}
            </div>
          </NavLink>
        </div>

        <div
          id={'scroll-container-' + id}
          className={'overflow-auto mt-2 px-2 w-full'}
          style={{
            scrollbarWidth: 'thin',
            scrollbarColor: '#333 #111827',
            height: 'calc(100% - 120px)',
          }}
        >
          {isLoading ? (
            <div className={'flex flex-col gap-2'}>
              <Skeleton
                variant={'rectangular'}
                className={'rounded-lg'}
                animation='wave'
                width={'100%'}
                height={40}
              />
              <Skeleton
                variant={'rectangular'}
                className={'rounded-lg'}
                animation='wave'
                width={'100%'}
                height={40}
              />
            </div>
          ) : (
            <InfiniteScroll
              className={'space-y-1'}
              dataLength={chats.length}
              next={() => {
                console.log(
                  offsetPage,
                  'offsetPage',
                  'chatsLength: ' + chats.length,
                )
                setOffsetPage(() => chats.length)
              }}
              hasMore={chats.length < chatsCount}
              loader={
                <Skeleton
                  variant={'rectangular'}
                  className={'rounded-lg'}
                  animation='wave'
                  width={'100%'}
                  height={40}
                />
              }
              scrollableTarget={'scroll-container-' + id}
            >
              {chats.map((chat) => {
                return (
                  <NavLink
                    className={({ isActive }) =>
                      cn(
                        'text-md group flex gap-2 items-center p-2 w-full justify-start font-medium cursor-pointer hover:text-white hover:bg-white/10 rounded-lg transition',
                        isActive ? 'text-white bg-white/10' : 'text-zinc-400',
                      )
                    }
                    key={chat.hash}
                    to={'/chats' + (chat.hash ? '/' + chat.hash : '')}
                    end
                  >
                    <div
                      className={
                        'overflow-hidden text-ellipsis text-nowrap w-full text-start'
                      }
                    >
                      {chat.title}
                    </div>
                    <Tooltip
                      title='Archive chat'
                      placement={'right'}
                      TransitionComponent={Zoom}
                      TransitionProps={{
                        timeout: {
                          appear: 1000,
                          enter: 1000,
                          exit: 200,
                        },
                      }}
                    >
                      <ArchiveIcon
                        className={'h-5 w-5'}
                        sx={{
                          color: 'transparent',
                          '&:hover': {
                            color: 'white',
                          },
                        }}
                        onClick={(e) =>
                          handleArchive(e, (chat as unknown as IChat)._id)
                        }
                        aria-label='delete'
                        color={'action'}
                      />
                    </Tooltip>
                  </NavLink>
                )
              })}
            </InfiniteScroll>
          )}
        </div>
      </div>
    </div>
  )
}

export default Sidebar
