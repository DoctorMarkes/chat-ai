import { useEffect, useState } from 'react'
import MenuIcon from '@mui/icons-material/Menu'
import Sidebar from '@components/Sidebar'
import { Box, Drawer, IconButton } from '@mui/material'

const MobileSidebar = () => {
  const [isMounted, setIsMounted] = useState(false)
  const [open, setOpen] = useState(false)

  const toggleDrawer = (newOpen: boolean) => () => {
    setOpen(newOpen)
  }

  useEffect(() => {
    setIsMounted(true)
  }, [])

  if (!isMounted) return null

  return (
    <>
      <Box className={'md:hidden'}>
        <IconButton
          onClick={toggleDrawer(true)}
          className={'hover:bg-black hover:text-sky-500'}
        >
          <MenuIcon />
        </IconButton>
      </Box>
      <Drawer open={open} onClose={toggleDrawer(false)}>
        <Sidebar />
      </Drawer>
    </>
  )
}

export default MobileSidebar
