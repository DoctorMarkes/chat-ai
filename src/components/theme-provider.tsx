import React from 'react'
import {
  createTheme,
  CssBaseline,
  ThemeProvider as MaterialThemeProvider,
} from '@mui/material'

type ThemeProviderProps = {
  children: React.ReactNode
}

const darkTheme = createTheme({
  palette: {
    mode: 'dark',
  },
})

export function ThemeProvider({ children }: ThemeProviderProps) {
  return (
    <MaterialThemeProvider theme={darkTheme}>
      <CssBaseline />
      {children}
    </MaterialThemeProvider>
  )
}
