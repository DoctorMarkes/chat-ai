import React from 'react'
import ReactDOM from 'react-dom/client'
import App from './app/App.tsx'
import {
  ClerkProvider,
  RedirectToSignIn,
  SignedIn,
  SignedOut,
  SignIn,
  SignUp,
} from '@clerk/clerk-react'
import { BrowserRouter, Route, Routes, useNavigate } from 'react-router-dom'
import { AppPage } from './pages/appPage/ui'
import { Box } from '@mui/material'

const clerkPubKey = import.meta.env.VITE_CLERK_PUBLISHABLE_KEY

const ClerkProviderWithRoutes = () => {
  const navigate = useNavigate()

  return (
    <ClerkProvider publishableKey={clerkPubKey} navigate={(to) => navigate(to)}>
      <Routes>
        <Route path={'/'} element={<AppPage />} />
        <Route
          path='/sign-in/*'
          element={
            <Box className={'flex items-center justify-center h-full'}>
              <SignIn redirectUrl={'/chats'} routing='path' path='/sign-in' />
            </Box>
          }
        />
        <Route
          path='/sign-up/*'
          element={
            <Box className={'flex items-center justify-center h-full'}>
              <SignUp redirectUrl={'/chats'} routing='path' path='/sign-up' />
            </Box>
          }
        />
        <Route
          path='/chats/:chatId?'
          element={
            <>
              <SignedIn>
                <App />
              </SignedIn>
              <SignedOut>
                <RedirectToSignIn />
              </SignedOut>
            </>
          }
        />
      </Routes>
    </ClerkProvider>
  )
}

ReactDOM.createRoot(document.getElementById('root')!).render(
  <React.StrictMode>
    <BrowserRouter>
      <ClerkProviderWithRoutes />
    </BrowserRouter>
  </React.StrictMode>,
)
