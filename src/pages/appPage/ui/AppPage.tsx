import { Box, Button, Link } from '@mui/material'
import { useAuth } from '@clerk/clerk-react'
import { useNavigate } from 'react-router-dom'

export const AppPage = () => {
  const { isSignedIn } = useAuth()

  const navigate = useNavigate()

  if (isSignedIn) navigate('/chats')

  return (
    <Box className={'flex h-full items-center justify-center gap-4'}>
      <Button variant='contained' component={Link} href={'/sign-in'}>
        Log in
      </Button>
      <Button variant='contained' component={Link} href={'/sign-up'}>
        Sign up
      </Button>
    </Box>
  )
}
