import React, {
  FC,
  FunctionComponent,
  PropsWithChildren,
  useEffect,
  useRef,
  useState,
} from 'react'
import { Avatar, Box, OutlinedInput } from '@mui/material'
import ArrowUpwardIcon from '@mui/icons-material/ArrowUpward'
import LoadingButton from '@mui/lab/LoadingButton'
import RadioButtonCheckedIcon from '@mui/icons-material/RadioButtonChecked'
import remarkGfm from 'remark-gfm'
import Markdown from 'react-markdown'
import {
  useAddMessageMutation,
  useFetchChatQuery,
} from '../../../services/chat.ts'
import { useNavigate, useParams } from 'react-router-dom'
import { CodeBlock, dracula } from 'react-code-blocks'
import { unshiftChats } from '../../../GlobalRedux/slices/chatSlice.ts'
import { useAppDispatch } from '../../../GlobalRedux/hook.ts'
import { useUser } from '@clerk/clerk-react'
import IntrinsicElements = React.JSX.IntrinsicElements

const P: FC<PropsWithChildren> = ({ children, ...rest }) => (
  <p className={'flex flex-col gap-2'} {...rest}>
    {children}
  </p>
)

export const ChatPage: FunctionComponent = () => {
  const [message, setMessage] = useState('')
  const params = useParams()
  const navigate = useNavigate()
  const [addMessage, { isLoading }] = useAddMessageMutation()
  const { data: chat } = useFetchChatQuery(params?.chatId ?? '')
  const dispatch = useAppDispatch()
  const { user } = useUser()

  const handleChange = (
    e: React.ChangeEvent<HTMLTextAreaElement | HTMLInputElement>,
  ) => {
    setMessage(e.target.value)
  }

  const handleSubmit = async (
    e: React.FormEvent<HTMLFormElement | HTMLDivElement | HTMLTextAreaElement>,
  ) => {
    e.preventDefault()
    if (!message) return
    setMessage('')
    const response = await addMessage({
      id: params?.chatId || '',
      message,
    })
    if (!params?.chatId && 'data' in response) {
      dispatch(unshiftChats({ chat: response.data }))
      navigate(`/chats/${response.data.hash}`)
    }
  }

  const handleKeyDown = async (e: React.KeyboardEvent<HTMLInputElement>) => {
    if (e.key === 'Enter') {
      await handleSubmit(e)
    }
  }

  const messagesEndRef = useRef<HTMLDivElement>(null)

  const scrollToBottom = () => {
    messagesEndRef?.current?.scrollIntoView()
  }

  useEffect(() => {
    scrollToBottom()
  }, [chat])

  return (
    <Box className={'h-full'}>
      <Box
        className={
          'flex flex-col gap-3 h-full md:pt-[64px] pt-[72px] pb-8 box-border'
        }
      >
        <Box
          sx={{
            scrollbarWidth: 'thin',
            scrollbarColor: '#333 #000',
            '&::-webkit-scrollbar': {
              width: '4px',
            },
            '&::-webkit-scrollbar-thumb': {
              backgroundColor: '#333',
              borderRadius: '3px',
            },
            '&::-webkit-scrollbar-track': {
              backgroundColor: '#222',
            },
          }}
          className={'mt-auto overflow-y-auto'}
        >
          {chat?.messages?.map((message) => {
            return (
              <>
                <Box
                  key={message._id}
                  className={'p-3 flex items-start gap-4 max-w-[900px] mx-auto'}
                >
                  <Avatar
                    sx={{ width: 24, height: 24 }}
                    alt='avatar'
                    src={
                      message.role !== 'user'
                        ? '/logo.png'
                        : user?.hasImage
                          ? user?.imageUrl
                          : ''
                    }
                  />
                  <Box className={'markdown-container w-[calc(100%-48px)]'}>
                    <Markdown
                      className={'text-start flex flex-col gap-2'}
                      remarkPlugins={[remarkGfm]}
                      components={{
                        p: P as unknown as keyof IntrinsicElements,
                        code(props) {
                          const { children, className } = props
                          const match = /language-(\w+)/.exec(className || '')
                          return (
                            <CodeBlock
                              text={children?.toString()}
                              language={match?.[1]}
                              theme={dracula}
                              showLineNumbers={false}
                            />
                          )
                        },
                      }}
                    >
                      {message.content}
                    </Markdown>
                  </Box>
                </Box>
                <div ref={messagesEndRef} />
              </>
            )
          })}
        </Box>
        <Box className={'w-full max-w-[900px] mx-auto px-4'}>
          <form noValidate autoComplete='off' onSubmit={handleSubmit}>
            <OutlinedInput
              size='small'
              placeholder='Сообщение ChatAI...'
              fullWidth
              className={'flex items-center gap-2'}
              disabled={isLoading}
              value={message}
              onChange={handleChange}
              multiline
              autoFocus
              onKeyDown={handleKeyDown}
              endAdornment={
                <LoadingButton
                  className={'min-w-1'}
                  size='small'
                  loading={isLoading}
                  variant='contained'
                  type={'submit'}
                >
                  {isLoading ? <RadioButtonCheckedIcon /> : <ArrowUpwardIcon />}
                </LoadingButton>
              }
            />
          </form>
        </Box>
      </Box>
    </Box>
  )
}
