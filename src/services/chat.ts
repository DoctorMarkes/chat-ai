import { createApi, fetchBaseQuery } from '@reduxjs/toolkit/query/react'
import { IChat } from '../types/chat.ts'

const baseUrl = import.meta.env.VITE_CLERK_BACKEND_URL

function getCookie(cookieName: string) {
  const name = `${cookieName}=`
  const decodedCookie = decodeURIComponent(document.cookie)
  const cookieArray = decodedCookie.split(';')

  if (!Array.isArray(cookieArray)) return null

  for (let i = 0; i < cookieArray.length; i++) {
    const cookie = cookieArray[i].trim()

    if (cookie.indexOf(name) === 0) {
      return cookie.substring(name.length, cookie.length)
    }
  }

  return null
}

export const chatsApi = createApi({
  reducerPath: 'chatsApi',
  baseQuery: fetchBaseQuery({
    baseUrl,
    credentials: 'include',
    prepareHeaders: (headers) => {
      headers.set('Authorization', `Bearer ${getCookie('__session')}`)
      return headers
    },
  }),
  tagTypes: ['Chats', 'Chat', 'ChatsCount'],
  endpoints: (builder) => ({
    fetchChats: builder.query<IChat[], { offsetPage: number }>({
      providesTags: ['Chats'],
      query: (req) => {
        const url = new URL(baseUrl + '/chats')
        if (req?.offsetPage)
          url.searchParams.set('offsetPage', req.offsetPage.toString())
        return url.toString()
      },
      transformErrorResponse: () => 'ошибка сервера',
    }),
    fetchChatsCount: builder.query<number, void>({
      query: () => `/chats/count`,
      transformErrorResponse: () => 'ошибка сервера',
      providesTags: ['ChatsCount'],
    }),
    fetchChat: builder.query<IChat, string>({
      query: (id) => `/chats/${id}`,
      transformErrorResponse: () => 'ошибка сервера',
      providesTags: ['Chat'],
    }),
    addMessage: builder.mutation<IChat, { id: string; message: string }>({
      query: ({ id, message }) => {
        return {
          url: '/chats',
          method: 'POST',
          body: {
            id,
            message,
          },
        }
      },
      invalidatesTags: ['Chat', 'ChatsCount'],
    }),
    archiveChat: builder.mutation<IChat, string>({
      query: (id) => {
        return {
          url: `/chats/${id}`,
          method: 'DELETE',
        }
      },
      invalidatesTags: ['ChatsCount'],
    }),
  }),
})

export const {
  useArchiveChatMutation,
  useFetchChatsCountQuery,
  useFetchChatsQuery,
  useFetchChatQuery,
  useAddMessageMutation,
} = chatsApi
