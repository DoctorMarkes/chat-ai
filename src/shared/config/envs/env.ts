export const NODE_ENV = process.env.NODE_ENV
export const isDevEnv = NODE_ENV === 'development'
export const isProdEnv = NODE_ENV === 'production'
