import { ComponentType, PropsWithChildren } from 'react'

export enum RouteName {
  CHAT_LIST_PAGE = '/chats',
  CHAT_PAGE = '/chats/:chatId',
}

export interface RouteDescription {
  path: RouteName
  component: ComponentType
  layout?: ComponentType<PropsWithChildren>
}
