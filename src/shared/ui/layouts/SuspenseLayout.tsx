import { FC, PropsWithChildren, Suspense } from 'react'

export const SuspenseLayout: FC<PropsWithChildren> = ({ children }) => {
  return <Suspense>{children}</Suspense>
}
