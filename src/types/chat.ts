export interface IChat {
  _id: string
  title: string
  hash: string
  userId: string
  isArchived: boolean
  messages?: Imessage[]
  createdAt: string
  updatedAt: string
  archivedAt: string
}

export interface Imessage {
  _id: string
  content: string
  role: 'user' | 'assistant' | 'system'
  chatId: string
}
